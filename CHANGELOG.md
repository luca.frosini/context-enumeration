This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for Infrastructure Contexts Enumerator


## [v2.0.0-SNAPSHOT] - 

- Switched JSON management to gcube-jackson [#19116]


## [v1.0.0-SNAPSHOT]

- First Version

