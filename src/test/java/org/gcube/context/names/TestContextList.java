package org.gcube.context.names;

import java.io.File;
import java.util.LinkedHashMap;
import java.util.Map;

import org.gcube.context.authorization.TokensUtil;
import org.gcube.resourcemanagement.support.server.managers.context.ContextManager;
import org.gcube.resourcemanagement.support.shared.types.datamodel.D4SEnvironment;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TestContextList extends ContextTest {
	
	private static final Logger logger = LoggerFactory.getLogger(TokensUtil.class); 
	
	@Test
	public void test() throws Exception {
		File src = new File("src");
		File test = new File(src, "test");
		File resources = new File(test, "resources");
		
		File outputFile = new File(resources, "output.txt");
		outputFile.delete();
		
		ContextList contextList = new ContextList(outputFile);
		contextList.all();
		
	}
	
	
	@Test
	public void generateTokens() throws Exception {
		Map<String, String> tokens = TokensUtil.getTokenForContexts("luca.frosini", "gCat");
		logger.debug("{}", tokens);
		
	}
	
	@Test
    public void getAllContexts() throws Exception { 
        LinkedHashMap<String, D4SEnvironment> contextsMap = ContextManager.readContextsWithUUIDs();
        for (String key : contextsMap.keySet()) {
            System.out.println("key:" + key + " **|||** " + contextsMap.get(key));

        }
    } 
}
