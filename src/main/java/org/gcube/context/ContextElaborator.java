package org.gcube.context;

import java.util.LinkedHashMap;
import java.util.SortedSet;
import java.util.TreeSet;

import org.gcube.resourcemanagement.support.server.managers.context.ContextManager;
import org.gcube.resourcemanagement.support.shared.types.datamodel.D4SEnvironment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public abstract class ContextElaborator {
	
	protected Logger logger = LoggerFactory.getLogger(this.getClass());
	
	public ContextElaborator(){
		
	}
	
	public void all() throws Exception{
		try {
			LinkedHashMap<String, D4SEnvironment> contexts = ContextManager.readContextsWithUUIDs();
			SortedSet<String> orderedContextNames = new TreeSet<>(contexts.keySet()); 
	        for (String context : orderedContextNames) {
	        	D4SEnvironment d4sEnvironment = contexts.get(context);
	        	try {
					logger.debug("Going to elaborate {}", context);
					elaborateContext(d4sEnvironment);
				}catch (Exception e) {
					logger.error("Error while elaborating {}", context, e);
					throw e;
				}
	        }
		} catch (Exception ex) {
			throw ex;
		}
	}

	protected abstract void elaborateContext(D4SEnvironment d4sEnvironment) throws Exception;
}
