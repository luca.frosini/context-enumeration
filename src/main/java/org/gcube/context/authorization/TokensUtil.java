package org.gcube.context.authorization;

import static org.gcube.common.authorization.client.Constants.authorizationService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.gcube.common.authorization.library.provider.SecurityTokenProvider;
import org.gcube.common.authorization.library.provider.UserInfo;
import org.gcube.resourcemanagement.support.server.managers.context.ContextManager;
import org.gcube.resourcemanagement.support.shared.types.datamodel.D4SEnvironment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TokensUtil {

	private static final Logger logger = LoggerFactory.getLogger(TokensUtil.class); 

	
	public static Map<String, String> getTokenForContexts(String username, String serviceName) throws Exception{
		Map<String, String> contextTokenMap = new HashMap<String, String>();
		
		/*
		ObjectMapper objectMapper = new ObjectMapper();
		ObjectNode objectNode = objectMapper.createObjectNode();
		*/
		
		try {
			
			LinkedHashMap<String, D4SEnvironment> contexts = ContextManager.readContextsWithUUIDs();
			for(String context : contexts.keySet()) {
				try {
					//D4SEnvironment d4sEnvironment = contexts.get(context);
					System.out.println("Going to generate Token for Context " + context);
					logger.info("Going to generate Token for Context {}", context);
					UserInfo userInfo = new UserInfo(username, new ArrayList<>());
					String userToken = authorizationService().generateUserToken(userInfo, context);
					SecurityTokenProvider.instance.set(userToken);
					String generatedToken = authorizationService().generateExternalServiceToken(serviceName);
					contextTokenMap.put(context, generatedToken);
					
					//objectNode.put(context, generatedToken);
					
					logger.info("Token for Context {} is {}", context, generatedToken);
				}catch (Exception e) {
					logger.error("Error while elaborating {}", context, e);
					throw e;
				} finally {
					SecurityTokenProvider.instance.reset();
				}
			}
		} catch (Exception ex) {
			throw ex;
		}
		
		/*
		File tokenFile = new File("tokens-"+serviceName+".json");
		objectMapper.writeValue(tokenFile, objectNode);
		*/
		
		return contextTokenMap;
	}
	
	public static void main(String[] args) throws Exception {
		if(args.length!=2) {
			System.out.println("Please provide 'vo file path' as first argument 'username' as second and 'external application name' as third");
			return;
		}
		getTokenForContexts(args[1], args[2]);
	}

}
