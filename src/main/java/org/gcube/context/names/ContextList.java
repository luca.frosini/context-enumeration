package org.gcube.context.names;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import org.gcube.context.ContextElaborator;
import org.gcube.resourcemanagement.support.shared.types.datamodel.D4SEnvironment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ContextList extends ContextElaborator {
	
	protected Logger logger = LoggerFactory.getLogger(ContextList.class);
	
	protected File outputFile;
	
	public ContextList(File outputFile) {
		this.outputFile = outputFile;
	}
	
	public static void printLine(File file, String line) throws Exception {
		synchronized (file) {
			try (FileWriter fw = new FileWriter(file, true);
					BufferedWriter bw = new BufferedWriter(fw);
					PrintWriter out = new PrintWriter(bw)) {
				out.println(line);
				out.flush();
			} catch (IOException e) {
				throw e;
			}
		}
	}
	
	
	@Override
	protected void elaborateContext(D4SEnvironment d4sEnvironment) throws Exception {
		printLine(outputFile, d4sEnvironment.getContext().toString());
	}

}
